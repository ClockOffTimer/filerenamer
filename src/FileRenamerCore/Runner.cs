﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;
using System.IO;
using System.Threading.Tasks;
using FileRenamer.CmdLine;
using FileRenamer.Properties;

namespace FileRenamer
{
    public static class Runner
    {
        private static Options options = default;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            TaskScheduler.UnobservedTaskException +=
                new EventHandler<UnobservedTaskExceptionEventArgs>(TaskScheduler_UnobservedTaskException);

            try
            {
                options = CmdOption.Parse<Options>(args);
                options.Check();

                if (options.IsCsScriptList)
                {
                    foreach (var d in Directory.EnumerateFiles(
                        options.ScriptDir.FullName, $"*{Options.ScriptFileExtension}", SearchOption.TopDirectoryOnly))
                    {
                        FileInfo f = new(d);
                        if (f != default)
                            Console.WriteLine($"\t- {f.Name} ({f.Length} byte, {f.LastWriteTime})");
                    }
                    ExitHandler();
                    return;
                }
                if (options.IsTestPatern)
                {
                    options.Filter.MaxFileCount = 100;
                    options.Filter.TestPattern();
                    ExitHandler();
                    return;
                }
                options.Begin();
            }
            catch (Exception ex) when (ex is CmdOptionException exc)
            {
                if (exc.IsCallHelp)
                {
                    Console.WriteLine($"{Environment.NewLine}\tUsing: {nameof(FileRenamer)}.exe <options>{Environment.NewLine}");
                    CmdOption.Help<Options>((a) => Resources.ResourceManager.GetString(a));
                }
                Console.WriteLine($"{Environment.NewLine}! {ToException(ex)}");
            }
            catch (Exception ex) {
                Console.WriteLine($"! {ToException(ex)}");
            }
            finally
            {
                if (!options.IsCsScriptList && !options.IsTestPatern && (options.Filter != default))
                    Console.WriteLine(
                        string.Format(Properties.Resources.F1,
                            Environment.NewLine,
                            options.Total,
                            options.Reanamed,
                            options.Errors));
            }
            ExitHandler();
        }

        private static void ExitHandler()
        {
            if ((options == default) || options.IsAutoClose)
                return;
            Console.WriteLine($"{Environment.NewLine}{Properties.Resources.M1}");
            Console.ReadKey();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) =>
            Console.WriteLine($"! {((Exception)e.ExceptionObject).Message}");

        private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e) =>
            Console.WriteLine($"! {e.Exception.Message}");

#       if DEBUG
        private static string ToException(Exception ex) => ex.ToString();
#       else
        private static string ToException(Exception ex) => ex.Message;
#       endif
    }
}
