﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System.IO;
using System.Reflection;
using System.Threading;
using FileRenamer.CmdLine;
using FileRenamer.Filter;

namespace FileRenamer
{
    public class Options
    {
        public const string ScriptDirConstant = "Filter";
        public const string ScriptFileExtension = ".csscript";

        public IFilter Filter  { get; set; } = default;
        public string  Current { get; set; } = string.Empty;
        public string  FileExt => (Filter != null) ? Filter.FileExt : string.Empty;

        private long _Total = 0L;
        private long _Errors = 0L;
        private long _Reanamed = 0L;

        public int Total { get => (int)Interlocked.Read(ref _Total); set => Interlocked.Exchange(ref _Total, value); }
        public int Errors { get => (int)Interlocked.Read(ref _Errors); set => Interlocked.Exchange(ref _Errors, value); }
        public int Reanamed { get => (int)Interlocked.Read(ref _Reanamed); set => Interlocked.Exchange(ref _Reanamed, value); }

        public void TotalIncrement() => Interlocked.Increment(ref _Total);
        public void ErrorsIncrement() => Interlocked.Increment(ref _Errors);
        public void ReanamedIncrement() => Interlocked.Increment(ref _Reanamed);

        [CmdOption(Key = "--script", IsFile = true, IsFileExists = true, FileStringFormat = @"Filter\{0}.csscript",
            ResourceId = "H1")]
        public FileInfo CsScript { get; set; } = default;

        [CmdOption(Key = "--script-dir", IsDirectory = true, IsDirectoryExists = true,
            ResourceId = "H2")]
        public DirectoryInfo ScriptDir { get; set; } = default;

        [CmdOption(Key = "--dir", IsDirectory = true, IsDirectoryExists = true,
            ResourceId = "H3")]
        public DirectoryInfo SrcDir { get; set; } = default;

        [CmdOption(Key = "--copyto", IsDirectory = true, IsDirectoryExists = false,
            ResourceId = "H8")]
        public DirectoryInfo DstDir { get; set; } = default;

        [CmdOption(Key = "--rw", IsSwitch = true,
            ResourceId = "H9")]
        public bool IsOverride { get; set; } = false;

        [CmdOption(Key = "--test", IsSwitch = true,
            ResourceId = "H4")]
        public bool IsTestMode { get; set; } = false;

        [CmdOption(Key = "--test-pattern", IsSwitch = true,
            ResourceId = "H5")]
        public bool IsTestPatern { get; set; } = false;

        [CmdOption(Key = "--close", IsSwitch = true,
            ResourceId = "H6")]
        public bool IsAutoClose { get; set; } = false;

        [CmdOption(Key = "--list", IsSwitch = true,
            ResourceId = "H7")]
        public bool IsCsScriptList { get; set; } = false;

        [CmdOption(Key = "--assembly-list", IsSwitch = true,
            ResourceId = "H10")]
        public bool IsDebugAssemblyLoad { get; set; } = false;

        public void Check()
        {
            if ((CsScript == default) && !IsCsScriptList)
                throw CmdOptionException.Create(
                    new FileNotFoundException(Properties.Resources.O3), true);

            if (ScriptDir == default)
                ScriptDir = new DirectoryInfo(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), ScriptDirConstant));

            if ((ScriptDir == default) || !ScriptDir.Exists)
                throw CmdOptionException.Create(
                    new DirectoryNotFoundException((ScriptDir == null) ? Properties.Resources.O2 : ScriptDir.FullName), true);

            if (IsCsScriptList)
                return;

            if ((SrcDir == default) && !IsTestPatern)
                throw CmdOptionException.Create(
                    new DirectoryNotFoundException(Properties.Resources.O1), true);

            Filter = new ScriptBuilder(CsScript, ScriptDir, IsDebugAssemblyLoad);
            if (Filter == default)
                throw CmdOptionException.Create(
                    new DirectoryNotFoundException(Properties.Resources.O4), true);

            if ((Filter.PluginType == ScriptType.TypeCopy) && (DstDir == default) && !IsTestPatern)
                throw CmdOptionException.Create(
                    new DirectoryNotFoundException(Properties.Resources.O5), true);
        }
    }
}
