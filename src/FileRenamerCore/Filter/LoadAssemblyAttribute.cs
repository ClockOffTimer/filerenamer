﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;

namespace FileRenamer.Filter
{
    /// <summary>
    /// тип пути для загрузки библиотеки
    /// </summary>
    public enum AssemblyPlace : int
    {
        /// <summary>
        /// нет
        /// </summary>
        None = 0,
        /// <summary>
        /// Докальная директория приложения
        /// </summary>
        LocalDirectory,
        /// <summary>
        /// Системная директория NET платформы
        /// </summary>
        SystenDirectory
    }

    /// <summary>
    /// Загрузить внешнюю сборку (.dll) из скрипта, target = Class/Assembly,
    /// разрешено использывать несколько экземпляров
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Assembly, AllowMultiple = true)]
    public class LoadAssemblyAttribute : Attribute
    {
        /// <summary>
        /// тип пути для загрузки библиотеки
        /// <see cref="AssemblyPlace">Enum AssemblyPlace</see>
        /// </summary>
        public readonly AssemblyPlace PlaceType;
        /// <summary>
        /// полное имя файла загружаемой библиотеки
        /// </summary>
        public readonly string FileName;
        /// <summary>
        /// Конструктор атрибута
        /// </summary>
        /// <param name="s">полное имя файла, если необходимо, включая путь</param>
        public LoadAssemblyAttribute(string s) { FileName = s; PlaceType = AssemblyPlace.LocalDirectory; }
        /// <summary>
        /// Конструктор атрибута
        /// </summary>
        /// <param name="s">полное имя файла, если необходимо, включая путь</param>
        /// <param name="p">тип пути <see cref="AssemblyPlace">Enum AssemblyPlace</see></param>
        public LoadAssemblyAttribute(string s, AssemblyPlace p) { FileName = s; PlaceType = p; }
    }
}
