﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Microsoft.CodeAnalysis;

namespace FileRenamer.Filter
{
    #region Enum Script Type
    /// <summary>
    /// Типы взаимодействия скрипта: Copy, Move, NoWrite
    /// в режиме TypeNoWrite предпологается что все действия с файлом
    /// будут призведены внутри скрипта.
    /// </summary>
    public enum ScriptType : int
    {
        /// <summary>
        /// Скрипт считается пустым (неработоспособным)
        /// </summary>
        None = 0,
        /// <summary>
        /// Скрипт ожидает переноса файлов
        /// </summary>
        TypeMove,
        /// <summary>
        /// Скрипт ожидает копирования файлов
        /// </summary>
        TypeCopy,
        /// <summary>
        /// Действия с файлом должны происходить внутри скрипта
        /// </summary>
        TypeNoWrite
    }
    #endregion

    #region Class File(s) Tags
    /// <summary>
    /// Класс контейнера <see cref="FileTags">медиа-тегов</see>
    /// </summary>
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class FilesTags
    {
        /// <summary>
        /// Список <see cref="FileTags">медиа-тегов</see>
        /// </summary>
        [XmlElement("tags")]
        public List<FileTags> Tags { get; set; } = new();
        /// <summary>
        /// Количество <see cref="FileTags">медиа-тегов</see> в коллекции
        /// </summary>
        [XmlIgnore]
        public int Count { get => Tags.Count; }
        /// <summary>
        /// Добавление <see cref="FileTags">медиа-тега</see>
        /// </summary>
        /// <param name="tag"><see cref="FileTags">медиа-тег</see></param>
        public void Add(FileTags tag) => Tags.Add(tag);
        /// <summary>
        /// Добавление <see cref="FileTags">медиа-тегов</see>
        /// </summary>
        /// <param name="tags">список IEnumerable <see cref="FileTags">медиа-тегов</see></param>
        public void AddRange(IEnumerable<FileTags> tags) => Tags.AddRange(tags);
        /// <summary>
        /// Очистка <see cref="FileTags">медиа-тегов</see>
        /// </summary>
        public void Clear() => Tags.Clear();
    }
    /// <summary>
    /// Класс медиа-тегов
    /// </summary>
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class FileTags
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        [XmlElement("file")]
        public string FileName { get; set; } = string.Empty;
        /// <summary>
        /// Тег "Артист"
        /// </summary>
        [XmlElement("artists")]
        public string Artists { get; set; } = string.Empty;
        /// <summary>
        /// Тег "Альбом"
        /// </summary>
        [XmlElement("album")]
        public string Album { get; set; } = string.Empty;
        /// <summary>
        /// Тег "Заголовок"
        /// </summary>
        [XmlElement("title")]
        public string Title { get; set; } = string.Empty;
        /// <summary>
        /// Тег "Описание"
        /// </summary>
        [XmlElement("desc")]
        public string Description { get; set; } = string.Empty;
        /// <summary>
        /// Тег "Группа/Жанр"
        /// </summary>
        [XmlElement("genre")]
        public string Genre { get; set; } = string.Empty;
        /// <summary>
        /// Тег "Номер диска"
        /// </summary>
        [XmlElement("disc")]
        public int Disc { get; set; } = -1;
        /// <summary>
        /// Тег "Номер трека"
        /// </summary>
        [XmlElement("track")]
        public int Track { get; set; } = -1;
        /// <summary>
        /// Тег "Количество трека"
        /// </summary>
        [XmlElement("count")]
        public int TrackCount { get; set; } = -1;
        /// <summary>
        /// Тег "Дата создания или модификации"
        /// </summary>
        [XmlElement("date")]
        public DateTime Date { get; set; } = default;
        /// <summary>
        /// Тег "Год создания или модификации"
        /// </summary>
        [XmlIgnore]
        public int Year { get => (Date == default) ? -1 : Date.Year; }
        /// <summary>
        /// Проверка незаполненного экземпляра
        /// </summary>
        [XmlIgnore]
        public bool IsEmpty => string.IsNullOrWhiteSpace(Album) && string.IsNullOrWhiteSpace(Artists) &&
            string.IsNullOrWhiteSpace(Genre) && string.IsNullOrWhiteSpace(Title) && string.IsNullOrWhiteSpace(Description) &&
            (Disc == -1) && (Track == -1);
        /// <summary>
        /// Конвертор int To uint
        /// </summary>
        /// <param name="s">имя свойства <see cref="string">string</see></param>
        /// <returns></returns>
        public uint Uint(string s) {
            try { return (uint) this.GetType().GetProperty(s).GetValue(this, null); }
            catch { return 0U; }
        }
        public override string ToString() {
            StringBuilder sb = new();
            if (!string.IsNullOrWhiteSpace(Album))
                sb.AppendLine($"\tAlbum = {Album}");
            if (!string.IsNullOrWhiteSpace(Artists))
                sb.AppendLine($"\tArtists = {Artists}");
            if (!string.IsNullOrWhiteSpace(Genre))
                sb.AppendLine($"\tGenre = {Genre}");
            if (!string.IsNullOrWhiteSpace(Title))
                sb.AppendLine($"\tTitle = {Title}");
            if (!string.IsNullOrWhiteSpace(Description))
                sb.AppendLine($"\tDescription = {Description}");
            if (Disc >= 0)
                sb.AppendLine($"\tDisc = {Disc}");
            if (Track >= 0)
                sb.AppendLine($"\tTrack = {Track}");
            if (TrackCount >= 0)
                sb.AppendLine($"\tTrackCount = {TrackCount}");
            if (Date != default)
                sb.AppendLine($"\tDate = {Date}");
            return sb.ToString();
        }
    }
    #endregion

    /// <summary>
    /// Базовый класс реализующий интерфейс IFilter
    /// <see cref="IFilter">Interface IFilter</see>.
    /// </summary>
    public class FilterBase : IFilter
    {
        /// <summary>
        /// Расширение файлов
        /// </summary>
        public string FileExt { get; protected set; } = string.Empty;

        /// <summary>
        /// Максимальное число файлов для переименования
        /// </summary>
        public int MaxFileCount { get => MaxFileCount_; set => MaxFileCount_ = value; }
        private int MaxFileCount_ = 0;

        /// <summary>
        /// Начальный индекс файлов для переименования
        /// </summary>
        public int StartFileCount { get; set; } = 0;

        /// <summary>
        /// Тип взаимодействия скрипта: Copy, Move, NoWrite
        /// <see cref="ScriptType">Enum ScriptType</see>
        /// </summary>
        public ScriptType PluginType { get; private set; } = ScriptType.None;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="ext">расширение файла, формат: ".ext"</param>
        /// <param name="type">тип взаимодействия скрипта <see cref="ScriptType">Enum ScriptType</see></param>
        public FilterBase(string ext, ScriptType type) { FileExt = ext; PluginType = type; }

        #region Parse
        /// <summary>
        /// Вызывается до на сканирования
        /// </summary>
        /// <param name="options">
        /// Общие настройки,
        /// <see cref="Options">Class Options</see>
        /// </param>
        public virtual void BeginParse(Options options = default) { }
        /// <summary>
        /// Обработка имени файла или строки описания.
        /// </summary>
        /// <param name="name">входная строка</param>
        /// <param name="options">Общие настройки <see cref="Options">Class Options</see></param>
        /// <returns>имя файла</returns>
        public virtual string Parse(string name, Options options = default) => throw new NotImplementedException();
        /// <summary>
        /// Вызывается в конце сканирования
        /// </summary>
        public virtual void EndParse() { }
        #endregion

        #region Regex pattern Filter/Test
        /// <summary>
        /// Проверка обработки выражения Regex.
        /// Позволяет протестировать работу фильтра без реального переименования файлов.
        /// Входящая строка для обработки береться из атрибута 'Example': [Example("входная строка")]
        /// </summary>
        public virtual void TestPattern() { }

        /// <summary>
        /// Проверка обработки выражения Regex, для использовании в классе 'Filter*'.
        /// </summary>
        /// <example>
        /// TestPattern() => TestPattern(this);
        /// </example>
        /// <param name="this">тип класса 'Filter*' откуда происходит вызов</param>
        protected void TestPattern(object @this)
        {
            try
            {
                List<string> list = GetExampleAttribute(@this);
                if ((list == null) || (list.Count == 0))
                    return;
                foreach(string s in list)
                    Console.WriteLine($"\n\tSRC: {s}\n\tDST: {Parse(Path.GetFileNameWithoutExtension(s))}\n");

            } catch (Exception e) { Console.WriteLine($"{nameof(GetExampleAttribute)} -> {e}"); }
        }

        /// <summary>
        /// Обработка выражения Regex.
        /// В случае несовпадения групп Regex с параметром count возвращается null.
        /// </summary>
        /// <param name="s">входящая строка</param>
        /// <param name="r">строка Regex</param>
        /// <param name="count">обязательное число вхождений групп Regex</param>
        /// <returns>результат обработки Regex в формате string [count]</returns>
        protected static string[] FilterRegex(string s, string r, int count)
        {
            Match m = Regex.Match(s, r,
                RegexOptions.CultureInvariant |
                RegexOptions.Singleline |
                RegexOptions.IgnoreCase |
                RegexOptions.Compiled);

            /*
            foreach (Group g in m.Groups)
            {
                Console.WriteLine($"{g.Value}/{g.Length}");
            }
            */

            if ((!m.Success) || (m.Groups.Count != count))
                return default;

            string[] ss = new string[count];
            for (int i = 0; i < count; i++)
                ss[i] = m.Groups[i].Value;
            return ss;
        }
        #endregion

        #region File name modify
        /// <summary>
        /// Обработка номера файла.
        /// Приводит номер к состоянию файлового индекса.
        /// Например: 1 => "001", 2 => "002", 55 => "055" и тд.
        /// </summary>
        /// <param name="num">номер</param>
        /// <returns></returns>
        protected static string FilterIndexNumber(int num) => (num < 10) ? $"00{num}" : ((num < 100) ? $"0{num}" : $"{num}");

        /// <summary>
        /// Конечная обработка имени файла.
        /// </summary>
        /// <param name="s">имя файла</param>
        /// <returns></returns>
        protected static string FilterFileName(string s) => s
            .Replace("<", "")
            .Replace(">", "")
            .Replace("«", "")
            .Replace("»", "")
            .Replace(",", "")
            .Replace("!", "")
            .Replace("#", "")
            .Replace("@", "")
            .Replace("$", "")
            .Replace("_", "")
            .Replace(":", "")
            .Replace(".", "")
            .Replace("    ", " ")
            .Replace("   ", " ")
            .Replace("  ", " ")
            .Trim();

        #endregion

        #region File media TAGs
        /// <summary>
        /// проверка возможности чтения/записи тегов
        /// в выбранный тип файла.
        /// </summary>
        /// <param name="path">путь к файлу <see cref="string">string</see></param>
        /// <returns>Boolean</returns>
        protected static bool FileTagsCheck(string path) =>
            FileTagsCheck(new FileInfo(path));
        /// <summary>
        /// проверка возможности чтения/записи тегов
        /// в выбранный тип файла.
        /// </summary>
        /// <param name="file">путь к файлу <see cref="FileInfo">Class FileInfo</see></param>
        /// <returns></returns>
        protected static bool FileTagsCheck(FileInfo file)
        {
            if ((file == default) || !file.Exists)
                return false;

            switch (file.Extension) {
                case ".avi":
                case ".m4a":
                case ".m4p":
                case ".m4v":
                case ".mkv":
                case ".mp3":
                case ".mp4":
                case ".mpg":
                case ".mpeg":
                case ".mpeg2":
                case ".mpeg-2":
                case ".wav":
                case ".wma":
                case ".wmv":
                case ".webm": return true;
                default: break;
            }
            return false;
        }

        /// <summary>
        /// запись тегов в файл
        /// </summary>
        /// <param name="path">путь к файлу</param>
        /// <param name="tags">класс тегов <see cref="FileTags">Class FileTags</see></param>
        protected static void FileTagsWrite(string path, FileTags tags)
        {
            if ((tags == default) || tags.IsEmpty)
                return;

            TagLib.File tfile = default;
            try {
                tfile = TagLib.File.Create(path);
                TagLibEmpty(tfile);

                if (!string.IsNullOrWhiteSpace(tags.Title))
                    tfile.Tag.Title = tags.Title;
                if (!string.IsNullOrWhiteSpace(tags.Artists))
                    tfile.Tag.AlbumArtists = new string[] { tags.Artists };
                if (!string.IsNullOrWhiteSpace(tags.Album))
                    tfile.Tag.Album = tags.Album;
                if (!string.IsNullOrWhiteSpace(tags.Description))
                    tfile.Tag.Description = tags.Description;
                if (!string.IsNullOrWhiteSpace(tags.Genre))
                    tfile.Tag.Genres = new string[] { tags.Genre };

                if (tags.Year > -1)
                    tfile.Tag.Year = tags.Uint(nameof(FileTags.Year));
                if (tags.Disc > -1)
                    tfile.Tag.Disc = tags.Uint(nameof(FileTags.Disc));
                if (tags.Track > -1)
                    tfile.Tag.Track = tags.Uint(nameof(FileTags.Track));
                if (tags.TrackCount > -1)
                    tfile.Tag.TrackCount = tags.Uint(nameof(FileTags.TrackCount));

                if (tags.Date != default)
                    tfile.Tag.DateTagged = tags.Date;

                tfile.Tag.Performers = Array.Empty<string>();
                tfile.Save();
            }
            catch (Exception ex) { InfoPrint($"! {path}", ex); }
            finally {
                if (tfile != default)
                    tfile.Dispose();
            }
        }
        /// <summary>
        /// чтение тегов из файла
        /// </summary>
        /// <param name="path">путь к файлу</param>
        /// <returns>класс тегов <see cref="FileTags">Class FileTags</see></returns>
        protected static FileTags FileTagsRead(string path)
        {
            FileTags tags = new();
            TagLib.File tfile = default;
            try {
                string s;
                StringBuilder sb = new();

                tfile = TagLib.File.Create(path);

                s = CheckTagString(tfile.Tag.Title);
                if (!string.IsNullOrWhiteSpace(s))
                    tags.Title = s;

                s = CheckTagString(tfile.Tag.Album);
                if (!string.IsNullOrWhiteSpace(s))
                    tags.Album = s;

                s = CheckTagString(tfile.Tag.Description);
                if (!string.IsNullOrWhiteSpace(s))
                    tags.Description = s;

                sb.Clear();
                if ((tfile.Tag.AlbumArtists != default) && (tfile.Tag.AlbumArtists.Length > 0)) {
                    foreach (string art in tfile.Tag.AlbumArtists)
                        sb.Append($"{art}, ");
                    tags.Artists = sb.ToString();
                }

                sb.Clear();
                if ((tfile.Tag.Genres != default) && (tfile.Tag.Genres.Length > 0)) {
                    foreach (string genre in tfile.Tag.Genres)
                        sb.Append($"{genre}, ");
                    tags.Genre = sb.ToString();
                }

                if (tfile.Tag.DateTagged != default)
                    tags.Date = (DateTime)tfile.Tag.DateTagged;
                else if ((tags.Date == default) && (tfile.Tag.Year > 0U))
                    tags.Date = new DateTime((int)tfile.Tag.Year, 1, 1);

                if (tfile.Tag.Disc > 0U)
                    tags.Disc = (int)tfile.Tag.Disc;
                if (tfile.Tag.Track > 0U)
                    tags.Track = (int)tfile.Tag.Track;
                if (tfile.Tag.TrackCount > 0U)
                    tags.TrackCount = (int)tfile.Tag.TrackCount;

            }
            catch (Exception ex) { InfoPrint($"! {path}", ex); }
            finally {
                if (tfile != default)
                    tfile.Dispose();
            }
            return tags;
        }
        /// <summary>
        /// Очистка всех тегов в файле
        /// </summary>
        /// <param name="path">путь к файлу <see cref="string">string</see></param>
        protected static void FileTagsEmpty(string path)
        {
            TagLib.File tfile = default;
            try {
                tfile = TagLib.File.Create(path);
                TagLibEmpty(tfile);
                tfile.Save();
            }
            catch (Exception ex) { InfoPrint($"! {path}", ex); }
            finally {
                if (tfile != default)
                    tfile.Dispose();
            }
        }
        /// <summary>
        /// Получает имя XML файла для директории (xml tags).
        /// </summary>
        /// <param name="options"><see cref="Options">Class Options</see></param>
        /// <returns><see cref="string">string</see></returns>
        protected static string GetXmlFile(Options options)
        {
            if ((options == default) || (options.SrcDir == default))
                return string.Empty;
            return GetXmlFile(options.SrcDir.FullName);
        }
        /// <summary>
        /// Получает имя XML файла для директории (xml tags).
        /// </summary>
        /// <param name="path"><see cref="string">string</see></param>
        /// <returns><see cref="string">string</see></returns>
        protected static string GetXmlFile(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;
            return Path.Combine(path,
                $"{Path.GetFileNameWithoutExtension(path)}.xmltags");
        }

        private static string CheckTagString(string s) =>
            string.IsNullOrWhiteSpace(s) ? default :
                ((Encoding.UTF8.GetByteCount(s) == s.Length) ? s : ConvertTagString(s));

        private static string ConvertTagString(string s)
        {
            try {
                byte[] bx = Encoding.Unicode.GetBytes(s);
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

                try {
                    UTF8Encoding utf8 = new(true, true);
                    int p = utf8.GetPreamble().Length;
                    _ = utf8.GetString(bx, p, bx.Length - p);
                    return s;
                }
                catch (DecoderFallbackException) {
                    try {
                        byte[] b0 = Encoding.Convert(
                            Encoding.Unicode,
                            Encoding.GetEncoding("latin1"),
                            bx); // *cp1251
                        byte[] b1 = Encoding.Convert(
                            Encoding.GetEncoding("windows-1251"),
                            Encoding.UTF8,
                            b0); // *utf8
                        return Encoding.UTF8.GetString(b1);
                    }
                    catch (Exception ex) { InfoPrint(ex); }
                    return string.Empty;
                }
            }
            catch (Exception ex) { InfoPrint(ex); }
            return string.Empty;
        }

        private static void TagLibEmpty(TagLib.File tfile)
        {
            tfile.Tag.Title =
            tfile.Tag.Album =
            tfile.Tag.Comment =
            tfile.Tag.Subtitle =
            tfile.Tag.AmazonId =
            tfile.Tag.Grouping =
            tfile.Tag.RemixedBy =
            tfile.Tag.AlbumSort =
            tfile.Tag.Conductor =
            tfile.Tag.Copyright =
            tfile.Tag.Description = string.Empty;

            tfile.Tag.Genres =
            tfile.Tag.Composers =
            tfile.Tag.Performers =
            tfile.Tag.PerformersRole =
            tfile.Tag.AlbumArtists = Array.Empty<string>();

            tfile.Tag.Year =
            tfile.Tag.Disc =
            tfile.Tag.Track =
            tfile.Tag.TrackCount =
            tfile.Tag.BeatsPerMinute = 0U;
        }
        #endregion

        #region Options Statistic helpers
        /// <summary>
        /// Добавление значений к статистики, свойство Total
        /// </summary>
        /// <param name="opt"><see cref="Options">Class Options</see></param>
        protected static void StatisticAddTotal(Options opt) { if (opt != default) opt.TotalIncrement(); }
        /// <summary>
        /// Добавление значений к статистики, свойство Reanamed
        /// </summary>
        /// <param name="opt"><see cref="Options">Class Options</see></param>
        protected static void StatisticAddReanamed(Options opt) { if (opt != default) opt.ReanamedIncrement(); }
        /// <summary>
        /// Добавление значений к статистики, свойство Errors
        /// </summary>
        /// <param name="opt"><see cref="Options">Class Options</see></param>
        protected static void StatisticAddErrors(Options opt) { if (opt != default) opt.ErrorsIncrement(); }
        #endregion

        #region Xml
        /// <summary>
        /// Сохранение данных в файл
        /// </summary>
        /// <typeparam name="T1">тип данных</typeparam>
        /// <param name="path">путь к файлу <see cref="string">string</see></param>
        /// <param name="src">данные</param>
        /// <param name="isoverride">перезаписывать файл</param>
        /// <param name="enc"><see cref="Encoding">Encoding</see></param>
        protected static void SerializeToFile<T1>(string path, T1 src, bool isoverride = false, Encoding enc = default)
        {
            if (src == null) return;
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using StreamWriter sw = new(path, isoverride, enc);
            XmlSerializer xml = new(typeof(T1));
            xml.Serialize(sw, src);
        }
        /// <summary>
        /// Чтение данных из файла
        /// </summary>
        /// <typeparam name="T1">тип данных</typeparam>
        /// <param name="path">путь к файлу <see cref="string">string</see></param>
        /// <param name="enc"><see cref="Encoding">Encoding</see></param>
        /// <returns>сщгласно типу данных</returns>
        protected static T1 DeserializeFromFile<T1>(string path, Encoding enc = default)
        {
            if (string.IsNullOrWhiteSpace(path)) return default;
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using StreamReader sr = new(path, enc, true);
            XmlSerializer xml = new(typeof(T1));
            if (xml.Deserialize(sr) is T1 val)
                return val;
            return default;
        }
        #endregion

        #region Console information print
        /// <summary>
        /// Печать в консль
        /// </summary>
        /// <param name="text">текст сообщения</param>
        protected static void InfoPrint(string text) => Console.WriteLine($"- {text}");
        /// <summary>
        /// Печать в консль
        /// </summary>
        /// <param name="ex">any exception</param>
        protected static void InfoPrint(Exception ex) => Console.WriteLine($"! {ToException(ex)}");
        /// <summary>
        /// Печать в консль
        /// </summary>
        /// <param name="text">текст сообщения</param>
        /// <param name="ex">any exception</param>
        protected static void InfoPrint(string text, Exception ex) => Console.WriteLine($"! {text} -> {ToException(ex)}");

#       if DEBUG
        private static string ToException(Exception ex) => ex.ToString();
#       else
        private static string ToException(Exception ex) => ex.Message;
#       endif
        #endregion

        #region Attribute / Reflection (ExampleAttribute)
        /// <summary>
        /// Используется для автоматической обработки атрибута 'Example': [Example("входная строка")]
        /// </summary>
        /// <param name="clz">тип класса</param>
        /// <returns></returns>
        protected List<string> GetExampleAttribute(object clz)
        {
            List<string> list = new();
            try {
                Type t = typeof(ExampleAttribute);
                foreach (var (prop, attr) in from MethodInfo mth in clz.GetType().GetMethods()
                                             from ExampleAttribute attr in mth.GetCustomAttributes(t, false)
                                             select (mth, attr))
                    list.Add(attr.Text);
            } catch (Exception e) { Console.WriteLine($"{nameof(GetExampleAttribute)} -> {e}"); }
            return list;
        }
        #endregion
    }
}
