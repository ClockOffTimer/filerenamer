﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;

namespace FileRenamer.Filter
{
    /// <summary>
    /// Определяет пример исходной строки для тестов, target = All,
    /// разрешено использывать несколько экземпляров
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class ExampleAttribute : Attribute
    {
        /// <summary>
        /// Исходная строка для теста
        /// </summary>
        public readonly string Text;
        /// <summary>
        /// Конструктор атрибута
        /// </summary>
        /// <param name="s">пример исходной строки для тестов</param>
        public ExampleAttribute(string s) => Text = s;
    }
}