﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

namespace FileRenamer.Filter
{
    /// <summary>
    /// Этот Интерфейс реализован в классе FilterBase, отдельной реализации в скрипте '.csscript' не требуется.
    /// Необходимо только унаследоваться от класса
    /// <see cref="FilterBase">Class FilterBase</see>.
    /// </summary>
    public interface IFilter
    {
        /// <summary>
        /// Типы взаимодействия скрипта: Copy, Move, NoWrite
        /// <see cref="ScriptType">Enum ScriptType</see>
        /// </summary>
        ScriptType PluginType { get; }
        /// <summary>
        /// Максимальное значение индекса для обработки = количество файлов.
        /// Используется при обратном переименовании.
        /// </summary>
        int MaxFileCount { get; set; }
        /// <summary>
        /// Значение количества файлов для Обработка начнется с данного индекса.
        /// Используется при любом переименовании.
        /// </summary>
        int StartFileCount { get; set; }
        /// <summary>
        /// Расширение файлов, только ни будут обработаны.
        /// </summary>
        string FileExt { get; }
        /// <summary>
        /// Вызывается до на сканирования
        /// </summary>
        /// <param name="options">
        /// Общие настройки,
        /// <see cref="Options">Class Options</see>
        /// </param>
        void BeginParse(Options options = default);
        /// <summary>
        /// Вызывается в конце сканирования
        /// </summary>
        void EndParse();
        /// <summary>
        /// В случае если PluginType == ScriptType.TypeNoWrite: полная обработка, любые операции с файлом.
        /// В других случаях преобразует входную строку, это имя файла без расширения, в выходное имя файла.
        /// </summary>
        /// <param name="name">
        /// В случае если PluginType == ScriptType.TypeNoWrite: полный путь и имя файла включая расширение.
        /// В других это имя файла без расширения.
        /// </param>
        /// <param name="options">
        /// Общие настройки,
        /// <see cref="Options">Class Options</see>
        /// </param>
        /// <see cref="FilterBase.Parse">Method FilterBase.Parse</see>
        /// <returns>новое имя файла</returns>
        string Parse(string name, Options options = default);
        /// <summary>
        /// Проверка обработки выражения Regex,
        /// <see cref="FilterBase.TestPattern()">Method FilterBase.TestPattern</see>
        /// </summary>
        void TestPattern();
    }
}