﻿using System;
using System.IO;

namespace FileRenamer.Filter
{
    [LoadAssemblyAttribute("TagLibSharp5.0.dll")]
    public class FileNameToMp3TagWriter : FilterBase
    {
        private static readonly Tuple<int, string>[] Rgx = new Tuple<int, string>[]
        {
            new Tuple<int, string>(7, @"^(?<index>\d+)\s-\s(?<author>[\w\s]+)\s-\s(?<title>.+)\s(?<pmajor>\d+)_(?<pminor>\d+)\.(?<ext>.+)$"),
            new Tuple<int, string>(6, @"^(?<index>\d+)\s-\s(?<author>[\w\s]+)\s-\s(?<title>.+)\s(?<part>\d+)\.(?<ext>.+)$"),
            new Tuple<int, string>(5, @"^(?<index>\d+)\s-\s(?<author>[\w\s]+)\s-\s(?<title>.+)\.(?<ext>.+)$"),
            new Tuple<int, string>(4, @"\s(?<author>[\w\s]+)\s-\s(?<title>.+)\.(?<ext>.+)$")
        };
        private static string Ext { get; } = @"*.mp3";
        private static ScriptType Typ { get; } = ScriptType.TypeNoWrite;
        private static bool DemoMode { get; set; } = false;

        public FileNameToMp3TagWriter() : base(Ext, Typ) { }
        public FileNameToMp3TagWriter(string _) : base(Ext, Typ) { }

        [Example("01 - TechDancer - Последние из могикан 1.mp3")]
        public override string Parse(string name, Options options)
        {

        FileNameToMp3TagWriter.InfoPrint($"> {name}");

            try {
                string[] ss = default;

                foreach (var t in Rgx) {
                    ss = FileNameToMp3TagWriter.FilterRegex(System.IO.Path.GetFileName(name), t.Item2, t.Item1);
                    if (ss != default) {
#                       if DEBUG
                        foreach (var s in ss)
                            Mp3NameToTagWriter.InfoPrint($"- {ss.Length}: {s}");
#                       endif
                        break;
                    }
                }
                if ((ss == default) || (ss.Length < 4)) {
                    FileNameToMp3TagWriter.InfoPrint($"? {name} = not regex matched");
                    return string.Empty;
                }
                if (!FileNameToMp3TagWriter.FileTagsCheck(name)){
                    FileNameToMp3TagWriter.InfoPrint($"! {name} = file not support extension");
                    return string.Empty;
                }

                FileTags tags = new() {
                    Title = ss[3],
                    Artists = ss[2],
                    Album = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(name)),
                    Date = DateTime.Now
                };

                switch (ss.Length) {
                    case 5: {
                            if (int.TryParse(ss[1], out int n))
                                tags.Track = n;
                            break;
                        }
                    case 6: {
                            if (int.TryParse(ss[1], out int n))
                                tags.Track = n;
                            if (int.TryParse(ss[4], out int t))
                                tags.Disc = t;

                            if (t > -1)
                                tags.Title = $"{tags.Title} {t}";
                            break;
                        }
                    case 7: {
                            if (int.TryParse(ss[1], out int n))
                                tags.Track = n;
                            if (int.TryParse(ss[4], out int t))
                                tags.Disc = t;
                            if (int.TryParse(ss[5], out int x))
                                tags.TrackCount = x;

                            if ((t > -1) && (x > -1))
                                tags.Title = $"{tags.Title} {t}.{x}";
                            else if (t > -1)
                                tags.Title = $"{tags.Title} {t}";
                            else if (x > -1)
                                tags.Title = $"{tags.Title} {x}";
                            break;
                        }
                }

                FileNameToMp3TagWriter.StatisticAddReanamed(options);
                FileNameToMp3TagWriter.InfoPrint($"= {name} = {ss.Length} Tags");

                if (DemoMode)
                    FileNameToMp3TagWriter.InfoPrint(tags.ToString());
                else
                    FileNameToMp3TagWriter.FileTagsWrite(name, tags);
            }
            catch (Exception ex) {
                FileNameToMp3TagWriter.InfoPrint($"! {name}", ex);
                FileNameToMp3TagWriter.StatisticAddErrors(options);
            }
            return string.Empty;
        }
        public override void BeginParse(Options options)
        {
            if ((options != default) && (options.IsTestMode || options.IsTestPatern))
                DemoMode = true;
        }

        public override void TestPattern() => TestPattern(this);
    }
}
