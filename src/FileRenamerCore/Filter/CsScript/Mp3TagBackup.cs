﻿
using System;
using System.IO;

namespace FileRenamer.Filter
{
    public class Mp3TagBackup : FilterBase
    {
        private const string Warn = "Фильтр сохранения тегов MP3 файлов в xml файле";
        private static string Ext { get; } = @"*.mp3";
        private static ScriptType Typ { get; } = ScriptType.TypeNoWrite;
        private readonly FilesTags Tags = new();
        private DirectoryInfo Dir = default;

        public Mp3TagBackup() : base(Ext, Typ) { }
        public Mp3TagBackup(string _) : base(Ext, Typ) { }

        [Example(Warn)]
        public override string Parse(string name, Options options)
        {
            Mp3TagBackup.InfoPrint($"+ {name}");
            FileTags tag = Mp3TagBackup.FileTagsRead(name);
            if (tag != default)
            {
                tag.FileName = Path.GetFileName(name);
                Tags.Add(tag);
                Mp3TagBackup.StatisticAddReanamed(options);
            }
            else
            {
                Mp3TagBackup.StatisticAddErrors(options);
            }
            return string.Empty;
        }
        public override void BeginParse(Options options)
        {
            if (options == default)
                throw new Exception("Options is null");
            if (options.SrcDir == default)
                throw new Exception("Directory handle is null");
            Dir = options.SrcDir;
        }
        public override void EndParse()
        {
            Mp3TagBackup.InfoPrint($"Save count: {Tags.Count}");
            if (Tags.Count == 0)
                return;
            if (Dir == default)
                throw new Exception("Output XML file name is empty");

            string path = Mp3TagBackup.GetXmlFile(Dir.FullName);
            Mp3TagBackup.InfoPrint($"Save file: {path}");
            Mp3TagBackup.SerializeToFile(path, Tags, true);
        }
        public override void TestPattern() => TestPattern(this);
    }
}
