﻿using System;
using System.IO;
using System.Linq;

namespace FileRenamer.Filter
{
    public class Mp3TagRestore : FilterBase
    {
        private const string Warn = "Фильтр востановления тегов из xml файла в MP3 файлы";
        private static string Ext { get; } = @"*.mp3";
        private static ScriptType Typ { get; } = ScriptType.TypeNoWrite;
        private FilesTags Tags = default;
        private FileInfo XmlFile = default;

        public Mp3TagRestore() : base(Ext, Typ) { }
        public Mp3TagRestore(string _) : base(Ext, Typ) { }

        [Example(Warn)]
        public override string Parse(string name, Options options)
        {
            Mp3TagRestore.InfoPrint($"+ {name}");
            string path = Path.GetFileName(name);
            FileTags tag = (from i in Tags.Tags
                            where i.FileName.Equals(path)
                            select i).FirstOrDefault();

            if (tag != default)
            {
                Mp3TagRestore.FileTagsWrite(name, tag);
                Mp3TagRestore.StatisticAddReanamed(options);
            }
            else
            {
                Mp3TagRestore.StatisticAddErrors(options);
            }
            return string.Empty;
        }
        public override void BeginParse(Options options)
        {
            if (options == default)
                throw new Exception("Options is null");

            string xmlfile = Mp3TagRestore.GetXmlFile(options);
            if (!string.IsNullOrWhiteSpace(xmlfile))
                XmlFile = new(xmlfile);
            if ((XmlFile == default) || !XmlFile.Exists)
                throw new Exception("Input XML file not found or name is empty");

            Tags = Mp3TagRestore.DeserializeFromFile<FilesTags>(XmlFile.FullName);
            if (Tags == default)
                throw new Exception("Input file not read data..");
            if (Tags.Count == 0)
                throw new Exception("Reading data is empty..");
        }
        public override void TestPattern() => TestPattern(this);
    }

}
