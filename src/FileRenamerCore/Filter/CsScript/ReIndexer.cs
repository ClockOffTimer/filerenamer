﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;
using System.Text;

namespace FileRenamer.Filter
{
    public class ReIndexer : FilterBase
    {
        private static string Rgx { get; set; } = @"^\[(?<number>\d+)\]\s\-\s(?<text>\S.*)$";
        private static string Ext { get; } = @"*.mp4";
        private static ScriptType Typ { get; } = ScriptType.TypeMove;
        private char[] trimChars = new char[] { ' ', '-', ',', '.', '\t', '\r', '\n' };
        private int current = 1;

        public ReIndexer() : base(Ext, Typ) { }
        public ReIndexer(string regex) : base(Ext, Typ) { Rgx = regex; }

        [Example("[001] - Закон Космической Справедливости I.mp4")]
        [Example("[002] - Закон Космической Справедливости II.mp4")]
        [Example("[006] - Закон Космической Справедливости III.mp4")]
        public override string Parse(string name, Options options) {
            try {
                do {
                    string[] ss = ReIndexer.FilterRegex(name, Rgx, 3);
                    if ((ss == null) || (ss.Length != 3)) {

                        ReIndexer.InfoPrint($"Not match pattern, skip file '{name}'");
                        break;
                    }

                    if (string.IsNullOrWhiteSpace(ss[2]))
                        break;

                    if (int.TryParse(ss[1], out int number)) {

                        if (number < 0)
                            break;

                        string txt = ReIndexer.FilterFileName(ss[2]);
                        if (string.IsNullOrWhiteSpace(txt))
                            break;
                        txt = txt.Normalize(NormalizationForm.FormC).Replace("  ", " ").Trim(trimChars);
                        return $"[{ReIndexer.FilterIndexNumber(current++)}] - {txt}";
                    }
                } while (false);
            }
            catch (Exception ex) { ReIndexer.InfoPrint(nameof(Parse), ex); }
            return string.Empty;
        }

        public override void TestPattern() => TestPattern(this);
    }
}
