﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;
using System.Text;

namespace FileRenamer.Filter
{
    public class NewIndex : FilterBase
    {
        private static string Rgx { get; set; } = @"^\s?\-?\s?(?<text>\S.*)$";
        private static string Ext { get; } = @"*.mp4";
        private static ScriptType Typ { get; } = ScriptType.TypeMove;
        private char[] trimChars = new char[] { ' ', '-', ',', '.', '\t', '\r', '\n' };
        private int current = 1;

        public NewIndex() : base(Ext, Typ) { }
        public NewIndex(string regex) : base(Ext, Typ) { Rgx = regex; }

        [Example("- Закон Космической Справедливости I.mp4")]
        [Example(" Закон Космической Справедливости II.mp4")]
        [Example("Закон Космической Справедливости III.mp4")]
        public override string Parse(string name, Options options) {
            try {
                do {
                    string[] ss = NewIndex.FilterRegex(name, Rgx, 2);
                    if ((ss == null) || (ss.Length != 2)) {

                        NewIndex.InfoPrint($"Not match pattern, skip file '{name}'");
                        break;
                    }

                    if (string.IsNullOrWhiteSpace(ss[1]))
                        break;

                    string txt = NewIndex.FilterFileName(ss[1]);
                    if (string.IsNullOrWhiteSpace(txt))
                        break;
                    txt = txt.Normalize(NormalizationForm.FormC).Replace("  ", " ").Trim(trimChars);
                    return $"[{NewIndex.FilterIndexNumber(current++)}] - {txt}";

                } while (false);
            }
            catch (Exception ex) { NewIndex.InfoPrint(nameof(Parse), ex); }
            return string.Empty;
        }

        public override void TestPattern() => TestPattern(this);
    }
}
