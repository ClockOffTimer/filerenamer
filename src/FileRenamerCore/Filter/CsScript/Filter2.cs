﻿using System;
using System.Text;

namespace FileRenamer.Filter
{
    public class Filter2 : FilterBase
    {
        private static string Rgx { get; set; } = @"^\[(?<number>\d+)\]\s\-\s(?<text>\S.*)$";
        private static string Ext { get; } = @"*.mp4";
        private static ScriptType Typ { get; } = ScriptType.TypeMove;

        public Filter2() : base(Ext, Typ) { }
        public Filter2(string regex) : base(Ext, Typ) { Rgx = regex; }

        [Example("[001] - КАК ГЛУБОКА КРОЛИЧЬЯ НОРА.mp4")]
        [Example("[002] - Вторая Эра II.mp4")]
        [Example("[006] - Галерея Мира Питера Ван Дер Аа.mp4")]
        public override string Parse(string name, Options options)
        {
            try {
                do {
                    string[] ss = Filter2.FilterRegex(name, Rgx, 3);
                    if ((ss == null) || (ss.Length != 3))
                    {
                        Filter2.InfoPrint($"Not match pattern, skip file '{name}'");
                        break;
                    }

                    if (string.IsNullOrWhiteSpace(ss[2]))
                        break;

                    if (int.TryParse(ss[1], out int number))
                    {
                        int num = MaxFileCount - number;
                        if (num < 0)
                            break;

                        string txt = Filter2.FilterFileName(ss[2]);
                        if (!string.IsNullOrWhiteSpace(txt))
                            return $"[{Filter2.FilterIndexNumber(num)}] - {txt.Normalize(NormalizationForm.FormC)}";
                    }
                } while (false);
            }
            catch (Exception ex) { Filter2.InfoPrint(nameof(Parse), ex); }
            return string.Empty;
        }

        public override void TestPattern() => TestPattern(this);
    }
}
