﻿using System;
using System.Text;

namespace FileRenamer.Filter
{
    public class Filter1 : FilterBase
    {
        private static string Rgx { get; set; } = @"^(\S.*)\sФильм\s(\d+)";
        private static string Ext { get; } = @"*.mp4";
        private static ScriptType Typ { get; } = ScriptType.TypeMove;

        public Filter1() : base(Ext, Typ) { }
        public Filter1(string regex) : base(Ext, Typ) { Rgx = regex; }

        [Example("Возникновение христианства в 15 веке Христианство и капитализм Фильм 5_720p.mp4")]
        public override string Parse(string name, Options options)
        {
            try {
                do {
                    string[] ss = Filter1.FilterRegex(name, Rgx, 3);
                    if ((ss == null) || (ss.Length != 3))
                    {
                        Filter1.InfoPrint($"Not match pattern, skip file '{name}'");
                        break;
                    }

                    if (string.IsNullOrWhiteSpace(ss[1]))
                        break;

                    if (int.TryParse(ss[2], out int num))
                    {
                        if (num < 0)
                            break;

                        string txt = Filter1.FilterFileName(ss[1]);
                        if (!string.IsNullOrWhiteSpace(txt))
                            return $"[{Filter1.FilterIndexNumber(num)}] - {txt.Normalize(NormalizationForm.FormC)}";
                    }
                } while (false);
            }
            catch (Exception ex) { Filter1.InfoPrint(nameof(Parse), ex); }
            return string.Empty;
        }

        public override void TestPattern() => TestPattern(this);
    }
}
