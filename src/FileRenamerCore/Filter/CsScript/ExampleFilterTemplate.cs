﻿
using System;

namespace FileRenamer.Filter
{
    [LoadAssemblyAttribute("MyLibrary.dll")]
    public class ExampleFilterTemplate : FilterBase
    {
        private const string Warn = "Тестовый фильтр заглушка - при запуске необходтмо выбрать рабочий фильтр";
        private static string Rgx { get; set; } = @"^\[(?<number>\d+)\]\s\-\s(?<text>\S.*)$";
        private static string Ext { get; } = @"*.txt";
        private static ScriptType Typ { get; } = ScriptType.TypeMove;

        public ExampleFilterTemplate() : base(Ext, Typ) { }
        public ExampleFilterTemplate(string regex) : base(Ext, Typ) => Rgx = regex;

        [Example(Warn)]
        public override string Parse(string name, Options options)
        {
            ExampleFilterTemplate.InfoPrint(Warn);

            string[] ss = ExampleFilterTemplate.FilterRegex(name, Rgx, 3);
            if (ss != default)
                foreach (var s in ss)
                    Console.WriteLine($"- {s}");
            return string.Empty;
        }

        public override void TestPattern() => TestPattern(this);
    }
}
