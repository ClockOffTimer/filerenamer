﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileRenamer.CmdLine;
using static System.Net.Mime.MediaTypeNames;

namespace FileRenamer
{
    public static class RenamerIO
    {
        #region IO Begin
        public static void Begin(this Options options)
        {
            if (options.Filter.PluginType == Filter.ScriptType.TypeCopy)
                Console.WriteLine($"{Environment.NewLine}\t{Properties.Resources.E8}: {options.SrcDir} -> {options.DstDir}{Environment.NewLine}");
            else
                Console.WriteLine($"{Environment.NewLine}\t{Properties.Resources.E8}: {options.SrcDir}{Environment.NewLine}");
            try {
                List<Tuple<string, string>> dlist = new();
                IEnumerable<string> slist = Directory.EnumerateFiles(options.SrcDir.FullName, options.Filter.FileExt, SearchOption.TopDirectoryOnly);
                options.Filter.MaxFileCount = slist.Count() + 1;
                options.Filter.BeginParse(options);

                foreach (string f1 in slist) {
                    options.Total++;
                    options.Current = f1;
                    string name = options.Filter.Parse((options.Filter.PluginType == Filter.ScriptType.TypeNoWrite) ?
                        f1 : Path.GetFileNameWithoutExtension(f1), options);
                    if (string.IsNullOrWhiteSpace(name))
                        continue;

                    dlist.Add(new Tuple<string, string>(f1, $"{name}{Path.GetExtension(f1)}"));

                    string[] flist = Directory.GetFiles(options.SrcDir.FullName, $"{name}*", SearchOption.TopDirectoryOnly);
                    if ((flist != null) && (flist.Length > 0)) {
                        foreach (string f2 in flist) {
                            try {
                                if (Path.GetExtension(f2).Equals(options.Filter.FileExt)) continue;
                                FileInfo fi = new(f2.Normalize(NormalizationForm.FormC));
                                if ((fi == null) || !fi.Exists) continue;

                                string fpath = Path.GetFileName(f2);
                                int idx = fpath.LastIndexOf(name);
                                if (idx++ <= 0) continue;
                                string fname = $"{name}{fpath.Substring(idx, fpath.Length - idx)}"
                                                             .Replace("___", "_")
                                                             .Replace("__", "_");
                                dlist.Add(new Tuple<string, string>(f2, $"{fname}{Path.GetExtension(f2)}"));
                            } catch (Exception ex) { Console.WriteLine($"! {nameof(RenamerIO)} -> {ex.Message}"); }
                        }
                    }
                }
                if (dlist.Count == 0)
                {
                    switch (options.Filter.PluginType) {
                        case Filter.ScriptType.TypeNoWrite: break;
                        case Filter.ScriptType.TypeCopy:
                        case Filter.ScriptType.TypeMove: {
                                Console.WriteLine($"{Environment.NewLine}{Properties.Resources.E7}");
                                break;
                            }
                        default: {
                                Console.WriteLine($"{Environment.NewLine}{Properties.Resources.E13}");
                                break;
                            }
                    }
                    return;
                }
                if (options.IsTestMode) {
                    foreach (Tuple<string, string> t in dlist)
                        Console.WriteLine($"+ {Path.GetFileNameWithoutExtension(t.Item1)}\n= {t.Item2}");
                }
                else {
                    switch (options.Filter.PluginType) {
                        case Filter.ScriptType.TypeNoWrite: break;
                        case Filter.ScriptType.TypeCopy: dlist.CopyFiles(options); break;
                        case Filter.ScriptType.TypeMove: dlist.RenameFiles(options); break;
                        default: Console.WriteLine($"{Environment.NewLine}{Properties.Resources.E13}"); break;
                    }
                }
            }
            finally {
                options.Filter.EndParse();
            }
        }
        #endregion

        #region IO Copy files
        private static void CopyFiles(this List<Tuple<string, string>> list, Options options)
        {
            try {
                if (options.DstDir == default)
                    throw CmdOptionException.Create(
                        new DirectoryNotFoundException(Properties.Resources.O5));

                if (!options.DstDir.Exists)
                    options.DstDir = Directory.CreateDirectory(options.DstDir.FullName);

                foreach (Tuple<string, string> t in list)
                {
                    FileInfo ifile = new(t.Item1.Normalize(NormalizationForm.FormC));
                    if (ifile.Exists)
                    {
                        if (ifile.IsReadOnly)
                            throw CmdOptionException.Create(
                                new FileLoadException($"{Properties.Resources.E3} {ifile.FullName}"));

                        options.Current = Path.Combine(options.DstDir.FullName, t.Item2);
                        FileInfo ofile = new(options.Current.Normalize(NormalizationForm.FormC));
                        if (ofile.Exists && !options.IsOverride)
                            throw CmdOptionException.Create(
                                new FileLoadException($"{Properties.Resources.E4} {ifile.FullName}"));

#                       if DEBUG
                        Console.WriteLine(ofile.FullName);
#                       else
                        ifile.CopyTo(ofile.FullName, options.IsOverride);
#                       endif
                        options.Reanamed++;
                    }
                    else
                        options.Errors++;
                }
            }
            catch { throw; }
            finally
            {
            }
        }
        #endregion

        #region IO Rename files
        private static void RenameFiles(this List<Tuple<string, string>> list, Options options)
        {
            DirectoryInfo dirTmp = default;
            try
            {
                string tmpdir = Guid.NewGuid().ToString();
                dirTmp = new(Path.Combine(options.SrcDir.FullName, tmpdir));
                if ((dirTmp == null) || dirTmp.Exists)
                    throw CmdOptionException.Create(
                        new DirectoryNotFoundException(Properties.Resources.E1));

                dirTmp.Create();
                dirTmp.Refresh();
                if (!dirTmp.Exists)
                    throw CmdOptionException.Create(
                        new DirectoryNotFoundException(Properties.Resources.E2));

                foreach (Tuple<string, string> t in list)
                {
                    FileInfo ifile = new(t.Item1.Normalize(NormalizationForm.FormC));
                    if (ifile.Exists)
                    {
                        if (ifile.IsReadOnly)
                            throw CmdOptionException.Create(
                                new FileLoadException($"{Properties.Resources.E3} {ifile.FullName}"));

                        options.Current = Path.Combine(dirTmp.FullName, t.Item2);
                        FileInfo ofile = new (options.Current.Normalize(NormalizationForm.FormC));
                        if (ofile.Exists && !options.IsOverride)
                            throw CmdOptionException.Create(
                                new FileLoadException($"{Properties.Resources.E4} {ifile.FullName}"));
#                       if DEBUG
                        Console.WriteLine(ofile.FullName);
#                       else
                        ifile.MoveTo(ofile.FullName, options.IsOverride);
#                       endif
                        options.Reanamed++;
                    }
                    else
                        options.Errors++;
                }
                foreach (string f in Directory.EnumerateFiles(dirTmp.FullName, options.Filter.FileExt, SearchOption.TopDirectoryOnly))
                {
                    FileInfo file = new(f.Normalize(NormalizationForm.FormC));
                    if ((file == null) || !file.Exists)
                        throw CmdOptionException.Create(
                            new FileNotFoundException($"{Properties.Resources.E5} {f}"));

                    string fn = Path.Combine(options.SrcDir.FullName, Path.GetFileName(file.FullName));
#                   if DEBUG
                    Console.WriteLine(fn);
#                   else
                    file.MoveTo(fn, options.IsOverride);
#                   endif
                }
            }
            catch { throw; }
            finally
            {
                if (dirTmp != default)
                {
                    dirTmp.Refresh();
                    if (dirTmp.Exists)
                    {
                        FileInfo[] fs = dirTmp.GetFiles();
                        if ((fs == null) || (fs.Length == 0))
                            dirTmp.Delete();
                        else
#                           pragma warning disable CA2219
                            throw CmdOptionException.Create(
                                new DirectoryNotFoundException(Properties.Resources.E6));
#                           pragma warning restore CA2219
                    }
                }
            }
        }
        #endregion
    }
}
