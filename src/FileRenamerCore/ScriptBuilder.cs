﻿/* Copyright (c) 2022 FileRenamer, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/filerenamer */

#if DEBUG
#define DEBUG_ASSEMBLY
#endif

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Loader;
using System.Text.RegularExpressions;
using FileRenamer.CmdLine;
using FileRenamer.Filter;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;

namespace FileRenamer
{
    public class ScriptBuilder : IFilter
    {
        private readonly object instance_;
        private readonly CSharpCompilation cmp_;
        private readonly MethodInfo Parse_;
        private readonly MethodInfo TestPattern_;
        private readonly MethodInfo BeginParse_;
        private readonly MethodInfo EndParse_;
        private readonly PropertyInfo MaxFileCount_;
        private readonly PropertyInfo StartFileCount_;
        private readonly PropertyInfo FileExt_;
        private readonly PropertyInfo PluginType_;

        private static bool IsDebugAssemblyLoad_;
        private static readonly string SysAssemblyPath_;
        static ScriptBuilder() => SysAssemblyPath_ = RuntimeEnvironment.GetRuntimeDirectory();

        public ScriptBuilder(FileInfo file, DirectoryInfo dir, bool IsDebugAssemblyLoad)
        {
            if ((file == null) || !file.Exists)
                throw CmdOptionException.Create(
                    new FileNotFoundException((file == null) ? nameof(file) : file.FullName));

            ScriptBuilder.IsDebugAssemblyLoad_ = IsDebugAssemblyLoad;
            string body = File.ReadAllText(file.FullName),
                   scriptname = Path.GetFileNameWithoutExtension(dir.FullName);
            List<MetadataReference> mref = MetadataReferences(SysAssemblyPath_, body);
            cmp_ = CSharpCompilation.Create($"{scriptname}Build")
                .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                .AddReferences(mref.ToArray())
                .AddSyntaxTrees(CSharpSyntaxTree.ParseText(body));

            foreach (Diagnostic msg in cmp_.GetDiagnostics())
                Console.WriteLine(msg);

            using MemoryStream ms = new();
            EmitResult emitResult = cmp_.Emit(ms);
            if (emitResult.Success)
            {
                ms.Seek(0, SeekOrigin.Begin);
                do {
                    AssemblyLoadContext context = AssemblyLoadContext.Default;
                    if (context == default)
                        break;
                    Assembly assembly = context.LoadFromStream(ms);
                    if (assembly == default)
                        break;

                    if (ScriptBuilder.IsDebugAssemblyLoad_) {
                        context.Resolving += (s, a) =>
                            { Console.WriteLine($"\tor+ {a?.Name}, {a?.Version}"); return default; };
                        assembly.ModuleResolve += (s, a) =>
                            { Console.WriteLine($"\tom+ {a?.Name}, {a?.RequestingAssembly.FullName}"); return default; };
                    }

                    instance_ = assembly.CreateInstance($"FileRenamer.{scriptname}.{Path.GetFileNameWithoutExtension(file.FullName)}");
                    if (instance_ == default)
                        break;
                    Type type = instance_.GetType();
                    if (type == default)
                        break;
                    PluginType_ = type.GetProperty(nameof(IFilter.PluginType));
                    if (PluginType_ == default)
                        break;
                    MaxFileCount_ = type.GetProperty(nameof(IFilter.MaxFileCount));
                    if (MaxFileCount_ == default)
                        break;
                    StartFileCount_ = type.GetProperty(nameof(IFilter.StartFileCount));
                    if (StartFileCount_ == default)
                        break;
                    FileExt_ = type.GetProperty(nameof(IFilter.FileExt));
                    if (FileExt_ == default)
                        break;
                    Parse_ = type.GetMethod(nameof(IFilter.Parse));
                    if (Parse_ == default)
                        break;
                    BeginParse_ = type.GetMethod(nameof(IFilter.BeginParse));
                    if (BeginParse_ == default)
                        break;
                    EndParse_ = type.GetMethod(nameof(IFilter.EndParse));
                    if (EndParse_ == default)
                        break;
                    TestPattern_ = type.GetMethod(nameof(IFilter.TestPattern));
                    if (TestPattern_ == default)
                        break;
                    return;

                } while (false);
                throw CmdOptionException.Create(
                    new NullReferenceException(nameof(CSharpCompilation)));
            }
        }

        private static List<string> MetadataReferencesFiles(string s)
        {
            string [] rgs = new string[]
            {
                "\\[LoadAssembly(:?Attribute|)\\(\\\"(?<name>[a-zA-Z0-9\\.]+)\\\"\\s?,\\s?AssemblyPlace\\.(?<place>[a-zA-Z0-9_]+)\\s?\\)\\]\\r?$",
                "\\[LoadAssembly(:?Attribute|)\\(\\\"(?<name>[a-zA-Z0-9\\.]+)\\\"\\)\\]\\r?$"
            };
            List<string> list = new();
            Regex[] rx = new Regex[] {
                new(rgs[0], RegexOptions.Multiline | RegexOptions.IgnoreCase |
                    RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture),
                new(rgs[1], RegexOptions.Multiline | RegexOptions.IgnoreCase |
                    RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture),
                };

            foreach (Regex r in rx) {

                MatchCollection matchs = r.Matches(s);

                if (matchs.Count > 0) {
                    string[] names = r.GetGroupNames();
                    string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                    foreach (Match m in matchs)
                    {
                        string slib = string.Empty,
                               splace = string.Empty;

                        foreach (string name in names)
                        {
                            if (name.Equals("name"))
                                slib = m.Groups[name].Value;
                            else if (name.Equals("place"))
                                splace = m.Groups[name].Value;
                        }
                        if (!string.IsNullOrWhiteSpace(slib))
                        {
                            AssemblyPlace type = AssemblyPlace.LocalDirectory;
                            if (!string.IsNullOrWhiteSpace(splace))
                                if (Enum.TryParse(typeof(AssemblyPlace), splace, true, out object obj))
                                    type = (AssemblyPlace)obj;

                            if (!slib.Contains('\\'))
                                slib = Path.Combine(
                                    (type == AssemblyPlace.LocalDirectory) ? path : SysAssemblyPath_, slib);
                            list.Add(slib);
#                           if DEBUG_ASSEMBLY
                            Console.WriteLine($"+ Add assembly to load: {type} = {slib}");
#                           endif
                        }
                    }
                }
            }
            return (list.Count == 0) ? default : list;
        }
        private static List<MetadataReference> MetadataReferences(string homedir, string body)
        {
            MetadataReference[] mref = new MetadataReference[] {
                MetadataReference.CreateFromFile(typeof(object).GetTypeInfo().Assembly.Location),
                MetadataReference.CreateFromFile(typeof(Console).GetTypeInfo().Assembly.Location),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "System.Configuration.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "System.Xml.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "System.Globalization.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "System.Runtime.Serialization.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "System.Collections.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "System.Diagnostics.Debug.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "mscorlib.dll")),
                MetadataReference.CreateFromFile(Path.Combine(homedir, "netstandard.dll"))
            };

            List<MetadataReference> lref = new(mref);
            List<PortableExecutableReference> loaded = (from i in AppDomain.CurrentDomain.GetAssemblies()
                                              where !i.IsDynamic && !string.IsNullOrWhiteSpace(i.Location)
                                              select MetadataReference.CreateFromFile(i.Location)).ToList();
            foreach (PortableExecutableReference r in loaded)
                if (!lref.Contains(r))
                    lref.Add(r);

            List<string> list = MetadataReferencesFiles(body);
            if ((list != default) && (list.Count > 0))
                foreach (string s in list)
                {
                    var a = (from n in lref
                             where n.Display.Equals(s)
                             select n).FirstOrDefault();
                    if (a == default)
                        lref.Add(MetadataReference.CreateFromFile(s));
                }
            if (ScriptBuilder.IsDebugAssemblyLoad_) {
                foreach (var v in lref)
                {
                    Console.WriteLine($"\t- {v.Display}");
                    if ((v.Properties != default) && (v.Properties.Aliases != default))
                        foreach (var p in v.Properties.Aliases)
                            Console.WriteLine($"\t\t- {p}");
                }
                AppDomain.CurrentDomain.AssemblyResolve += (s, a) =>
                    { Console.WriteLine($"\ta+ {a?.Name}, {a?.RequestingAssembly.FullName}, {a?.RequestingAssembly.Location}"); return default; };
                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += (s, a) =>
                    { Console.WriteLine($"\to+ {a?.Name}, {a?.RequestingAssembly.FullName}, {a?.RequestingAssembly.Location}"); return default; };
                AppDomain.CurrentDomain.ResourceResolve += (s, a) =>
                    { Console.WriteLine($"\tr+ {a?.Name}, {a?.RequestingAssembly.FullName}, {a?.RequestingAssembly.Location}"); return default; };
            }
            return lref;
        }

        public int MaxFileCount {
            get => (int)MaxFileCount_.GetValue(instance_);
            set => MaxFileCount_.SetValue(instance_, value);
        }
        public int StartFileCount {
            get => (int)StartFileCount_.GetValue(instance_);
            set => StartFileCount_.SetValue(instance_, value);
        }
        public ScriptType PluginType
        {
            get => (ScriptType)PluginType_.GetValue(instance_);
            set => PluginType_.SetValue(instance_, value);
        }
        public string FileExt => (string)FileExt_.GetValue(instance_);

        public void   TestPattern() => TestPattern_.Invoke(instance_, null);
        public void   BeginParse(Options options = null) => BeginParse_.Invoke(instance_, new object[] { options });
        public void   EndParse() => EndParse_.Invoke(instance_, null);
        public string Parse(string name, Options options = default)
        {
            object obj = Parse_.Invoke(instance_, new object[] { name, options });
            if (obj is string s)
                return s;
            return string.Empty;
        }
    }
}
