﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace FilterDesignTemplate
{
    internal static class FilterDesignProjecеUtils
    {
        public static string GetProjectPath(this string path)
        {
            if (!string.IsNullOrWhiteSpace(path))
                return path;
            return Assembly.GetExecutingAssembly().Location;
        }

        public static void AddDictionary(this Dictionary<string, string> dict, string key, string val)
        {
            if (dict.ContainsKey(key))
                dict.Remove(key);
            dict.Add(key, val.Trim());
        }

        public static void DumpDictionary(this Dictionary<string, string> dict, string path)
        {
            path = GetProjectPath(path);
            if (string.IsNullOrWhiteSpace(path))
                return;

            StringBuilder sb = new();
            foreach (var a in dict)
                sb.AppendLine($"{a.Key} = {a.Value}");
            File.WriteAllText(Path.Combine(path, "FilterDesignVariables.txt"), sb.ToString());
        }

        public static void WriteException(this Exception ex, string path = default)
        {
            if (ex == default)
                return;

            try
            {
                string file = string.Empty;
                if (string.IsNullOrWhiteSpace(path))
                {
                    path = GetProjectPath(path);
                    if (string.IsNullOrWhiteSpace(path))
                        return;
                    file = $"{Path.GetFileNameWithoutExtension(path)}-{ex.GetType().Name}.txt";
                }
                else
                {
                    file = $"{nameof(FilterDesignProjectWizard)}-{ex.GetType().Name}.txt";
                }
                File.WriteAllText(
                    Path.Combine(Path.GetDirectoryName(path), file),
                    ex.ToString());
            } catch { }
        }
    }
}
