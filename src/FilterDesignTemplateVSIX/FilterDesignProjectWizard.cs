﻿using System;
using System.Collections.Generic;
using System.IO;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using Microsoft.Win32;

namespace FilterDesignTemplate
{
    public class FilterDesignProjectWizard : IWizard
    {
        public void BeforeOpeningFile(ProjectItem projectItem) { }
        public void ProjectFinishedGenerating(Project project) { }
        public void ProjectItemFinishedGenerating(ProjectItem projectItem) { }
        public void RunFinished() { }
        public bool ShouldAddProjectItem(string filePath) => true;

        public void RunStarted(
            object aobj,
            Dictionary<string, string> dict,
            WizardRunKind kind,
            object[] custom)
        {
            _ = dict.TryGetValue("$destinationdirectory$", out string installPath);
            try {
                try {
                    do {
                        using RegistryKey skey = Registry.LocalMachine.OpenSubKey("SOFTWARE", false);
                        if (skey == default)
                            break;
                        using RegistryKey pkey = skey.OpenSubKey(@"NT\File Renamer");
                        if (pkey == default)
                            break;
                        string path = pkey.GetValue("Path").ToString();
                        if (string.IsNullOrWhiteSpace(path))
                            break;

                        dict.AddDictionary("$installrootpath$", path);
                        dict.AddDictionary("$installfilterpath$", Path.Combine(path.Trim(), "Filter"));

                    } while (false);
                } catch (Exception ex) { ex.WriteException(installPath); }

                if (dict.TryGetValue("$safeprojectname$", out string s))
                    dict.AddDictionary("$projecttag$", s.Replace("Filter", "").Replace(" ", ""));

                dict.DumpDictionary(installPath);

            } catch (Exception ex) { ex.WriteException(installPath); }
        }
    }
}
