﻿using System;
using System.Threading.Tasks;
using FileRenamer.Filter;

namespace Runner.NS$safeprojectname$
{
	class ProjectRunner
	{
		static void Main(string[] _)
		{
			try {
				AppDomain.CurrentDomain.UnhandledException +=
					new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
				TaskScheduler.UnobservedTaskException +=
					new EventHandler<UnobservedTaskExceptionEventArgs>(TaskScheduler_UnobservedTaskException);

				IFilter plugin = new Filter$projecttag$
				{
					MaxFileCount = 100
				};
				plugin.TestPattern();

			}
			catch (Exception ex) { Console.WriteLine(ex.ToString()); }
		}
		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) =>
			Console.WriteLine($"! {((Exception)e.ExceptionObject).Message}");
		private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e) =>
			Console.WriteLine($"! {e.Exception.Message}");
	}
}
