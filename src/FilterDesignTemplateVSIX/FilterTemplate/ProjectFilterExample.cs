﻿
namespace FileRenamer.Filter
{
    public class Filter$projecttag$ : FilterBase
    {
        private const string Warn = "Тестовый фильтр заглушка - при запуске выбор фильтров 1 или 2";
        private static string Ext { get; } = @"*.txt";

        public Filter$projecttag$() : base(Ext) { }
        public Filter$projecttag$(string _) : base(Ext) { }

        [Example(Warn)]
        public override string Parse(string name)
        {
            Filter$projecttag$.InfoPrint(Warn);
            return string.Empty;
        }
        public override void TestPattern() => TestPattern(this);
    }
}
