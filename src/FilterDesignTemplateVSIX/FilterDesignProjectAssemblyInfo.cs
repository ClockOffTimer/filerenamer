﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Filter design Template")]
[assembly: AssemblyDescription("C# script based File Renamer, all user plugin in C# source code")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NT")]
[assembly: AssemblyProduct("FilterDesignTemplate")]
[assembly: AssemblyCopyright("Copyright © NT 2021-2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.2.0.5")]
[assembly: AssemblyFileVersion("1.2.0.5")]
[assembly: Guid("e92394cd-c0d2-4cbf-80d9-52c26d00d29e")]
